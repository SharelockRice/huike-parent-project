package com.huike.interceptor;


import com.huike.common.constant.Constants;
import com.huike.common.constant.HttpStatus;
import com.huike.domain.system.dto.LoginUser;
import com.huike.web.service.TokenService;
import io.netty.handler.codec.MessageAggregationException;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.HttpComponentsAsyncClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.management.openmbean.OpenDataException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class LoginCheckInterceptor implements HandlerInterceptor {

    @Autowired
    private TokenService tokenService;



    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("拦截到了请求，{}",request.getRequestURI());

        // 1.获取请求头的令牌
        String token = tokenService.getToken(request);

        // 2.判断令牌是否存在，不存401
        if (!StringUtils.hasLength(token)){
            log.info("令牌为空，401");
            response.setStatus(HttpStatus.UNAUTHORIZED);
            return false;
        }

        // 3.校验令牌是否存在，错误401
        try {
            tokenService.parseToken(token);
        }catch (Exception e){
            e.printStackTrace();
            log.info("令牌非法，返回401");
            response.setStatus(HttpStatus.UNAUTHORIZED);
            return false;
        }

        // 4.获取redis中的用户登录信息
        LoginUser loginUser = tokenService.getLoginUser(request);
        if(loginUser==null){
            log.info("用户登录信息已过期");
            response.setStatus(HttpStatus.UNAUTHORIZED);
            return false;
        }

        // 5.放行
        // redis续期
        tokenService.refreshAndCacheToken(loginUser);
        return true;

    }
}
