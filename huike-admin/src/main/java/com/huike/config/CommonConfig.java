package com.huike.config;

import com.huike.common.config.MinioConfig;
import com.huike.utils.MinIOUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CommonConfig {
    // 声明工具类的bean
    @Bean
    public MinIOUtils minIOUtils(MinioConfig minioConfig){
        return new MinIOUtils(minioConfig);
    }
}
