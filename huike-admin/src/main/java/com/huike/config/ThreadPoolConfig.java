package com.huike.config;


import com.huike.common.annotation.PreAuthorize;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.annotation.PreDestroy;
import java.util.concurrent.*;

@Slf4j
@EnableAsync // 开启异步任务
@Configuration
public class ThreadPoolConfig {
//    ----------------------线程池的绑定,jdk线程池
    ThreadPoolExecutor threadPoolExecutor = null;
    @Bean("jdkThreadPool")
    public ThreadPoolExecutor threadPoolExecutor(){
        log.info("jdk线程池");
        threadPoolExecutor = new ThreadPoolExecutor(
                10, 20, 5, TimeUnit.MINUTES,
                new ArrayBlockingQueue<>(10),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy()     // 拒绝策略
        );

        return threadPoolExecutor;
    }
    ThreadPoolTaskExecutor threadPoolTaskExecutor = null;

    // ---------spring线程池
    @Bean("springThreadPool")
    public TaskExecutor taskExecutor(){
        log.info("spring线程池");
        threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(10);
        threadPoolTaskExecutor.setMaxPoolSize(20);
        threadPoolTaskExecutor.setKeepAliveSeconds(300);
        threadPoolTaskExecutor.setThreadNamePrefix("Huike-Thread-pool-");        // -----指定名字
        threadPoolTaskExecutor.setQueueCapacity(10);   // ----队列容量

        return threadPoolTaskExecutor;
    }

    @PreDestroy                  // 关闭线程池
    public void shutdown(){
        log.info("服务器关闭");
        if(threadPoolExecutor == null){
            threadPoolExecutor.shutdown();
            log.info("threadPoolExecutor线程池关闭");
        }
        if (threadPoolTaskExecutor == null){
            threadPoolTaskExecutor.shutdown();
            log.info("threadPoolTaskExecutor线程池关闭");
        }
    }
}
