package com.huike.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class MonthDetailReportTask {

    @Scheduled(cron  = "0 0 23 L * ?")
    public void sendMonthDetailEmail(){
        // 1.本月的开始时间，结束时间
        LocalDate begin = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth());
        LocalDate end = LocalDate.now().with(TemporalAdjusters.lastDayOfMonth());
        // 1.1 获取每一天存集合里
        List<String> collect = begin.datesUntil(end.plusDays(1)).map(localDate -> localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))).collect(Collectors.toList());

        // 2.查询数据库获取指定原始数据， -线索 商机 合同 销售额
        // 获取最大值，最小值，日期里
        LocalDateTime beginTime = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(end, LocalTime.MIN);
        // 2.1线索

        // 2.2商机

        // 2.3合同数量

        // 2.4销售额

        // 3.构建数据模型



    }


}
