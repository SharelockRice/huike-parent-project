package com.huike.aspectj;

import com.alibaba.fastjson.JSONObject;
import com.huike.common.annotation.Log;
import com.huike.common.exception.CustomException;
import com.huike.domain.system.SysOperLog;
import com.huike.domain.system.dto.LoginUser;
import com.huike.service.ISysOperLogService;
import com.huike.utils.ip.AddressUtils;
import com.huike.utils.ip.IpUtils;
import com.huike.web.service.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Date;

@Controller
@Aspect
@Slf4j
public class OperLogAspect {

    @Autowired
    private ISysOperLogService sysOperLogService;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private TokenService tokenService;

    @Around("execution(* com.huike.controller.*.*.*(..)) && @annotation(myLog)")
    public Object aspLog(ProceedingJoinPoint joinPoint, Log myLog) throws Throwable {
        Object result = null;
        try {

            result = joinPoint.proceed();  // 方法放行
            handlelog(joinPoint, myLog, result, null);               // 记录日志

        } catch (Throwable throwable) {
            throwable.printStackTrace();

            handlelog(joinPoint, myLog, null, throwable);
            throw new CustomException(throwable.getMessage());  // 继续往上抛
        }

        return result;
    }


    private void handlelog(ProceedingJoinPoint joinPoint, Log myLog, Object result, Throwable throwable) {
        SysOperLog sysOperLog = new SysOperLog();
        sysOperLog.setTitle(myLog.title());
        sysOperLog.setBusinessType(myLog.businessType().ordinal());   // ordinal拿到枚举类型的索引
        sysOperLog.setMethod(joinPoint.getTarget().getClass().getName() + "." + joinPoint.getSignature().getName());
        sysOperLog.setRequestMethod(request.getMethod());
        sysOperLog.setOperatorType(1);

        // 用户信息
        LoginUser loginUser = tokenService.getLoginUser(request);
        if (loginUser != null) {
            sysOperLog.setOperName(loginUser.getUser().getRealName());
            sysOperLog.setDeptName(loginUser.getUser().getDept() != null ? loginUser.getUser().getDept().getDeptName() : null);
        }

        sysOperLog.setOperIp(request.getRequestURI());
        // ip
        String ipAddr = IpUtils.getIpAddr(request);
        sysOperLog.setOperIp(ipAddr);

        // ip归属地
        String realAddressByIP = AddressUtils.getRealAddressByIP(ipAddr);
        sysOperLog.setOperLocation(realAddressByIP);

        // 参数，返回值
        sysOperLog.setOperParam(Arrays.toString(joinPoint.getArgs()));
        sysOperLog.setJsonResult(JSONObject.toJSONString(result));
        sysOperLog.setOperTime(new Date());
        sysOperLog.setStatus(0);

        // 异常判断
        if(throwable != null){
            sysOperLog.setStatus(1);
            sysOperLog.setErrorMsg(throwable.getMessage());
        }

        sysOperLogService.insertOperlog(sysOperLog);
    }


}
